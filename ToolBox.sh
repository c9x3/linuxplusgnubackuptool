#!/bin/bash



# Colors
ESC=$(printf '\033') RESET="${ESC}[0m" BLACK="${ESC}[30m" RED="${ESC}[31m"
GREEN="${ESC}[32m" YELLOW="${ESC}[33m" BLUE="${ESC}[34m" MAGENTA="${ESC}[35m"
CYAN="${ESC}[36m" WHITE="${ESC}[37m" DEFAULT="${ESC}[39m"



# Color Templates
greenprint() { printf "${GREEN}%s${RESET}\n" "$1"; }
blueprint() { printf "${BLUE}%s${RESET}\n" "$1"; }
redprint() { printf "${RED}%s${RESET}\n" "$1"; }
yellowprint() { printf "${YELLOW}%s${RESET}\n" "$1"; }
magentaprint() { printf "${MAGENTA}%s${RESET}\n" "$1"; }
cyanprint() { printf "${CYAN}%s${RESET}\n" "$1"; }



# Software Name
software_name='LinuxPlusGNUToolBox'
# Version Check
version_check='v1.2.4.6'



# START fn_clear
fn_clear() { clear; }
# END fn_clear



# START fn_exit
fn_exit() { exit; }
# END fn_exit



# START fn_toolbox_updater
fn_toolbox_updater() {
clear
echo ""
echo "⟡ $software_name Updater ⟡"
echo ""
echo "➤ Version Check: [⟡ $software_name $version_check ⟡]"
echo ""
echo "➤ Process started from..."
echo ""
pwd
echo ""
echo "➤ Attempting to backup the un-updated file..."
echo ""

mkdir $version_check

# cp and tell me whats happening.
cp -v LinuxPlusGNUToolBox.sh $version_check
mkdir LinuxPlusGNUToolBox_BKs
# cp recursively.
cp -r $version_check LinuxPlusGNUToolBox_BKs
cp -r LinuxPlusGNUToolBox_BKs ~/Documents/

echo ""
echo "➤ Attempting to update the file..."
echo ""
curl -o LinuxPlusGNUToolBox.sh https://gitlab.com/c9x3/LinuxPlusGNUToolBox/-/raw/main/LinuxPlusGNUToolBox.sh
echo ""
echo "➤ Adding executable permissions to LinuxPlusGNUToolBox.sh..."
chmod +x LinuxPlusGNUToolBox.sh
echo ""
echo "The update should have been completed successfully! Restart LinuxPlusGNUToolBox to confirm that everything has been updated! :)"
echo ""

# Cleanup.
rm -r LinuxPlusGNUToolBox/
rm -r LinuxPlusGNUToolBox_BKs/
rm -r $version_check

}
# END fn_toolbox_updater



# START fn_12-24hr_timetable
fn_12-24hr_timetable() {

clear

echo ""
echo "# A Twenty Four Hour Day:"
echo "
| 01:00AM | 05:00AM | 09:00AM | 01:00PM or 13:00 | 05:00PM or 17:00 | 09:00PM or 21:00 |
| 02:00AM | 06:00AM | 10:00AM | 02:00PM or 14:00 | 06:00PM or 18:00 | 10:00PM or 22:00 |
| 03:00AM | 07:00AM | 11:00AM | 03:00PM or 15:00 | 07:00PM or 19:00 | 11:00PM or 23:00 |
| 04:00AM | 08:00AM | 12:00PM | 04:00PM or 16:00 | 08:00PM or 20:00 | 12:00AM or 24:00 |" && echo ""

}
# END fn_12-24hr_timetable



# START fn_c2f
# Celsius to Fahrenheit Function
fn_c2f() {

clear
echo ""
echo "Input value (0-9, -0-9) to convert from Celsius to Fahrenheit."
echo ""
read -r 'var'
echo ""
echo "Celsius should be equivalent to..."
echo ""
echo "scale=5; 1.8 * $var + 32" | bc
echo ""
echo "Fahrenheit."
echo ""

}
# END fn_c2f



# START fn_f2c
# Fahrenheit to Celsius Function
fn_f2c() {

clear
echo ""
echo "Input value (0-9, -0-9) to convert from Fahrenheit to Celsius."
echo ""
read -r 'var'
echo ""
echo "Fahrenheit should be equivalent to..."
echo ""

var2=$(echo "scale=5; $var-32" | bc)

echo "scale=5; $var2 / 1.8" | bc
echo ""
echo "Celsius."
echo ""

}
# END fn_f2c



# START fn_temperature_converter
# Temperature Unit Converter Menu Function
fn_temperature_converter() {
clear
echo -ne "
$(greenprint '⟡ Unit Converter Command Menu ⟡')

$(greenprint '02)') Celsius to Fahrenheit
$(greenprint '01)') Fahrenheit to Celsius
$(greenprint '00)') Return to main menu

Choose an option:  "
read -r ans
case $ans in
2)
fn_c2f
;;
1)
fn_f2c
;;
0)
./LinuxPlusGNUToolBox.sh
;;
*)
fn_exit
;;
esac
}
# END fn_temperature_converter



# START fn_unit_converter_menu
# Unit Converter Menu Function
fn_unit_converter_menu() {
clear
echo -ne "
$(greenprint '⟡ Unit Converter Command Menu ⟡')

$(greenprint '02)') Temperature Converter
$(greenprint '01)') Time (UNIT) Converter
$(greenprint '00)') Return to main menu

Choose an option:  "
read -r ans
case $ans in
2)
fn_temperature_converter
;;
1)
fn_time_unit_converter_menu
;;
0)
./LinuxPlusGNUToolBox.sh
;;
*)
fn_exit
;;
esac
}
# END fn_unit_converter_menu



# START fn_return_to_menu
fn_return_to_menu() {
echo ""
read -r -p "⟡ Do you want to return to the main menu? If not, this process will be aborted. [yes/no or, y/n] ⟡ " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
echo "text"
else
echo ""
echo "➤ Process aborted..."
echo ""
exit
fi
}
# END fn_return_to_menu



# START fn_pwgen
fn_pwgen () {

#!/bin/bash

clear

echo ""
echo "Input desired number of characters [#s 0-9]"
echo ""

echo "# Your input:"
echo ""
read -r chosen_number
echo ""
echo "# Password:"
echo ""
tr -dc [['A-Za-z0-9!~`@#$%^&*(){}[]\|;:"<,.>?/_+ -']] < /dev/urandom | head -c $chosen_number
echo ""
echo ""

}
# STOP fn_pwgen



# START fn_worldtime_command_menu
fn_worldtime_command_menu() {
clear
echo -ne "
⟡ WorldTime Command Menu ⟡ 

$(greenprint '12)') Vietnam
$(greenprint '11)') Brazil
$(greenprint '10)') Australia
$(greenprint '09)') New_Zealand
$(greenprint '08)') Japan
$(greenprint '07)') China
$(greenprint '06)') Ukraine
$(greenprint '05)') Italy
$(greenprint '04)') United_Kingdom
$(greenprint '03)') New_York
$(greenprint '02)') Dallas_Texas
$(greenprint '01)') Los_Angeles

Use Ctrl+C end this script.
""
Press 'Enter' to start! "
read -r ans
case $ans in
*)
fn_worldtime
;;
esac
}
# Worldtime Function
fn_worldtime() {
#!/bin/bash
1() {
echo "# Current Time In Los_Angeles:" && echo "" && TZ=America/Los_Angeles date
}
2() {
echo "# Current Time In Dallas_Texas:" && echo "" && TZ=America/Chicago date
}
3() {
echo "# Current Time In New_York:" && echo "" && TZ=America/New_York date
}
4() {
echo "# Current Time In United_Kingdom:" && echo "" && TZ=Europe/London date
}
5() {
echo "# Current Time In Italy:" && echo "" && TZ=Europe/Rome date
}
6() {
echo "# Current Time In Ukraine:" && echo "" && TZ=Europe/Vilnius date
}
7() {
echo "# Current Time In China:" && echo "" && TZ=Asia/Shanghai date
}
8() {
echo "# Current Time In Japan:" && echo "" && TZ=Asia/Tokyo date
}
9() {
echo "# Current Time In New_Zealand:" && echo "" && TZ=Antarctica/McMurdo date
}
10() {
echo "# Current Time In Australia:" && echo "" && TZ=Australia/Sydney date
}
11() {
echo "# Current Time In Brazil:" && echo "" && TZ=America/Sao_Paulo date
}
12() {
echo "# Current Time In Vietnam:" && echo "" && TZ=Asia/Bangkok date
}
clear
# Select Time-Zone
echo ""
echo -n "# Input Desired Time-Zone: "
read -r time_zone
echo ""
echo "CONFIRMATION: $time_zone"
echo ""
# Countdown
#!/bin/bash
# $1 = # of seconds
# $@ = What to print after "Waiting (number of) seconds"
countdown() {
secs=$1
shift
msg=$@
while [ $secs -gt 0 ]
do
# Start
clear
echo ""
echo "# Countdown (trying to synchronize Time-Zones!)"
echo ""
printf $((secs--))
echo ""
echo ""
echo "# Use Ctrl+C end this script."
echo ""
sleep 1
done
echo
}
# Countdown [Seconds]
countdown 1
# Show Time-Zone
while :;
do
clear
echo "# Your Current Time:"
echo ""
date 
echo ""
$time_zone
echo ""
echo "# Use Ctrl+C end this script."
echo ""
sleep 1
done
echo ""
}
# END fn_worldtime_command_menu



# START fn_file_organizer
fn_file_organizer() {
echo ""
echo "➤ Process started from..."
echo ""
pwd
echo ""
echo "➤ If the process hangs, use CTRL+C to stop the process."
echo ""
echo "➤ Making directories..."
echo ""
mkdir -p Organized
cd Organized/
mkdir -p svg/
mkdir -p exe/
mkdir -p webm/
mkdir -p webp/
mkdir -p odt/
mkdir -p gif/
mkdir -p jpg/
mkdir -p mov/
mkdir -p mp3/
mkdir -p mp4/
mkdir -p png/
mkdir -p wav/
mkdir -p ogg/
mkdir -p midi/
mkdir -p midi/
mkdir -p wma/
mkdir -p aif/
mkdir -p m4v/
mkdir -p mkv/
mkdir -p txt/
mkdir -p pdf/
mkdir -p m4r/
mkdir -p caf/
mkdir -p m4a/
mkdir -p flac/
mkdir -p jar/
mkdir -p zip/
mkdir -p 7z/
mkdir -p rar/
cd ..
echo "➤ Moving files into directories..."
echo ""
mv --backup=numbered *.svg Organized/svg -v
mv --backup=numbered *.mp4 Organized/mp4 -v
mv --backup=numbered *.mp3 Organized/mp3 -v
mv --backup=numbered *.mov Organized/mov -v
mv --backup=numbered *.webp Organized/webp -v
mv --backup=numbered *.odt Organized/odt -v
mv --backup=numbered *.gif  Organized/gif -v
mv --backup=numbered *.jpg Organized/jpg -v
mv --backup=numbered *.jpeg Organized/jpg -v
mv --backup=numbered *.png Organized/png -v
mv --backup=numbered *.webm Organized/webm -v
mv --backup=numbered *.exe Organized/exe -v
mv --backup=numbered *.ogg Organized/ogg -v
mv --backup=numbered *.wav Organized/wav -v
mv --backup=numbered *.aif Organized/aif -v
mv --backup=numbered *.m4v Organized/m4v -v
mv --backup=numbered *.wma Organized/wma -v
mv --backup=numbered *.midi Organized/midi -v
mv --backup=numbered *.mid Organized/midi -v
mv --backup=numbered *.m4a Organized/m4a -v
mv --backup=numbered *.flac Organized/flac -v
mv --backup=numbered *.caf Organized/caf -v
mv --backup=numbered *.m4r Organized/m4r -v
mv --backup=numbered *.pdf Organized/pdf -v
mv --backup=numbered *.mkv Organized/mkv -v
mv --backup=numbered *.txt Organized/txt -v
mv --backup=numbered *.jar Organized/jar -v
mv --backup=numbered *.zip Organized/zip -v
mv --backup=numbered *.7z Organized/7z -v
mv --backup=numbered *.rar Organized/rar -v
cd Organized
echo ""
echo "➤ Removing empty directories..."
echo ""
cd ..
find . -type d -empty -delete -print
echo ""
echo "➤ Process completed successfully!"
echo ""
echo "➤ Don't forget to refresh your file manager!"
echo ""
echo "➤ Backed up files will be backed up with (~#~) behind the original file extension. These files should still work after being renamed. Backed up files MAY also be hidden so you may have to enable seeing hidden items in your file manager (showing hidden files in file managers is usually mapped to Ctrl+H.) Or use 'ls -a' to display all files (including hidden files!)"
echo ""
}
# END fn_file_organizer



# START fn_brightness_tool
fn_brightness_tool() {
# Use 'xrandr -q' to see what displays are connected (so you can configure this tool!)
# Select Brightness Value
echo ""
echo -n "# Input Desired Brightness Value [0.25 (25%) - 1.0 (100%) Brightness]: "
read -r lightvar
echo ""
echo "CONFIRMATION: $lightvar"
echo ""
xrandr --output HDMI-0 --brightness $lightvar
}
# END fn_brightness_tool



# START fn_bk_tool
fn_bk_tool() {
clear
# Create log file:
mkdir ~/Documents/WIPBKtool/
touch ~/Documents/WIPBKtool/Logs.txt
# Countdown Timer
#!/bin/bash
# $1 = # of seconds
# $@ = What to print after "Waiting (number of) seconds"
countdown() {
secs=$1
shift
msg=$@
while [ $secs -gt 0 ]
do
# Start
clear
echo "# Days, hours, minutes, and seconds until loop."
echo ""
printf '%dd:%dh:%dm:%ds\n' $((secs/86400)) $((secs%86400/3600)) $((secs%3600/60)) \
$((secs%60))
echo ""
echo "# Seconds until loop."
echo ""
printf $((secs--))
echo ""
echo "# Use Ctrl+C end this script."	
echo ""
sleep 1
done
echo
}
#
# Infinite bash loop. 
# This ensures the task is automated and runs until the user stops the process. 
# The default location for logs is, "~/Documents/WIPBKtool/Logs.txt" if you are skeptical of any errors. 
#
while true
do
#
# Clear logs and move the logs to the trash after 30 24 hour long days minus 60 seconds so that the logs get cleared. 
#
clear
watch -n 2591940 $(fn_clear_and_remove_logs)
echo ""
printf "Use Ctrl+C end this script."
echo ""
echo ""
echo "Script running from:"
echo ""
pwd
echo ""
echo "###" >> ~/Documents/WIPBKtool/Logs.txt
echo "START Time:" >> ~/Documents/WIPBKtool/Logs.txt
date >> ~/Documents/WIPBKtool/Logs.txt
echo "-- START log area. --" >> ~/Documents/WIPBKtool/Logs.txt
# Copy the Directory, "Dir1" to "Dir2" and make a log at specific directory.
cp -u -v -r -p ~/Documents/Dir1/ ~/Documents/Dir2/ >> ~/Documents/WIPBKtool/Logs.txt
echo "-- STOP log area. --" >> ~/Documents/WIPBKtool/Logs.txt
echo "STOP Time:" >> ~/Documents/WIPBKtool/Logs.txt
date >> ~/Documents/WIPBKtool/Logs.txt
echo "###" >> ~/Documents/WIPBKtool/Logs.txt
echo "" >> ~/Documents/WIPBKtool/Logs.txt
: '
The sequence of events that should transpire:
Countdown (duration of seconds) -> Sleep (duration of seconds) -> Repeat these sequence of events.
The duration counts down from the highest number to the lowest number.

# Units of time conversion:

60 seconds    = 1 minute.
3600 seconds  = 1 hour.
86400 seconds = 1 24 hour day.
604800 seconds = 7 24 hour days.
2592000 seconds = 30 24 hour days.
31536000 seconds = 365 24 hour days.
'
countdown 75 && sleep 8s
# The sleep command is run to give the logs time to refresh. 
done
}
# END fn_bk_tool



# START fn_time_converter_millennia
fn_time_converter_millennia() {
clear
# Countdown Timer
#!/bin/bash
# $"31557600000" = # of seconds in 1 24 hour day
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"31557600000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Millennia:"
echo ""
printf $((secs/31557600000))
echo ""
}
echo ""
echo -n "# Millennia: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_millennia



# START fn_time_converter_centuries
fn_time_converter_centuries() {
clear
# Countdown Timer
#!/bin/bash
# $"3153600000" = # of seconds in 1 Century
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"3153600000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Centuries:"
echo ""
printf $((secs/3153600000))
echo ""
}
echo ""
echo -n "# Centuries: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_centuries



# START fn_time_converter_decades
fn_time_converter_decades() {
clear
# Countdown Timer
#!/bin/bash
# $"315360000" = # of seconds in 1 decade 
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"315360000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Decades:"
echo ""
printf $((secs/315360000))
echo ""
}
echo ""
echo -n "# Decades: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_decades



# START fn_time_converter_years
fn_time_converter_years() {
clear
# Countdown Timer
#!/bin/bash
# $"31536000" = # of seconds in 1 365 24 hour day year
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"31536000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Days:"
echo ""
printf $((secs/31536000))
echo ""
}
echo ""
echo -n "# Years: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_years



# START fn_time_converter_months
fn_time_converter_months() {
clear
# Countdown Timer
#!/bin/bash
# $"2630000" = # of seconds in 1 month
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"2630000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Months:"
echo ""
printf $((secs/2630000))
echo ""
}
echo ""
echo -n "# Months: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_months



# START fn_time_converter_weeks
fn_time_converter_weeks() {
clear
# Countdown Timer
#!/bin/bash
# $"604800" = # of seconds in 7 24 hour days (1 week)
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"604800"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Weeks:"
echo ""
printf $((secs/604800))
echo ""
}
echo ""
echo -n "# Weeks: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_weeks



# START fn_time_converter_days
fn_time_converter_days() {
clear
# Countdown Timer
#!/bin/bash
# $"86400" = # of seconds in 1 24 hour day
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"86400"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Days:"
echo ""
printf $((secs/86400))
echo ""
}
#
echo ""
echo -n "# Days: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_days



# START fn_time_converter_hours
fn_time_converter_hours() {
clear
# Countdown Timer
#!/bin/bash
# $"3600" = # of seconds in 1 hour
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"3600"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Hours:"
echo ""
printf $((secs/3600))
echo ""
}
echo ""
echo -n "# Hours: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_hours



# START fn_time_converter_minutes
fn_time_converter_minutes() {
clear
# Countdown Timer
#!/bin/bash
# $"60" = # of seconds in 1 minute
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"60"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Minutes:"
echo ""
printf $((secs/86400))
echo ""
}
echo ""
echo -n "# Minutes: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_minutes



# START fn_time_converter_seconds
fn_time_converter_seconds() {
clear
# Countdown Timer
#!/bin/bash
# $"1" = # of seconds in 1 second
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"1"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000)) $((secs/3110400000)) $((secs/311040000)) $((secs/31536000)) $((secs/2592000)) $((secs/86400)) $((secs/3600)) $((secs/60)) $((secs)) $((secs*1000))
echo ""
echo "# Seconds:"
echo ""
printf $((secs/1))
echo ""
}
echo ""
echo -n "# Seconds: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_seconds



# START fn_time_converter_milliseconds 
fn_time_converter_milliseconds() {
clear
# Countdown Timer
#!/bin/bash
# $"1000" = # of milliseconds in 1 second
# $@ = What to print after "Waiting (number of) seconds"
timeconvert() {
secs=$"1000"*$days
shift
msg=$@
# Start
clear
echo ""
echo "- Unit Conversion Chart -"

printf '
Millennia: %d
Centuries: %d
Decades: %d
Years: %d
Months: %d
Days: %d
Hours: %d
Minutes: %d
Seconds: %d
Milliseconds: %d \n' \
\
\
\
$((secs/31104000000000000000)) $((secs/3110400000000000)) $((secs/311040000000000)) $((secs/31104000000000)) $((secs/2592000000000)) $((secs/86400000000)) $((secs/3600000000)) $((secs/60000000)) $((secs/1000000)) $((secs/1000))
echo ""
echo "# Milliseconds:"
echo ""
printf $((secs/1000))
echo ""
}
#
echo ""
echo -n "# Milliseconds: "
read -r days
echo "Seconds you inputted = $days"
timeconvert $days
echo ""
}
# END fn_time_converter_milliseconds



# START fn_manual
fn_manual() {
clear
echo ""
echo "⟡ PROJECT INFORMATION ⟡"
echo ""
echo "➤ Version Check: [⟡ $software_name $version_check ⟡]"
echo ""
echo "➤ OFFICIAL Repository: [https://gitlab.com/c9x3/LinuxPlusGNUToolBox]"
echo ""
echo "➤ Change-Log: [https://gitlab.com/c9x3/LinuxPlusGNUToolBox/-/blob/main/CHANGELOG.md]"
echo ""
echo "➤ Licensing and Donation Information: [https://c9x3.neocities.org/]"
echo ""
echo "⟡ MANUAL ⟡"
echo ""
echo "Welcome to LinuxPlusGNUToolBox! Upon starting this tool, you should be greeted with a command menu. Choose whichever option corresponds to your needs by typing the number matching the task that you would like to run."
echo ""
echo "[12-24 Hour Time Table]"
echo "option displays a 12 and 24 hour time table."
echo ""
echo "[Password Generator]"
echo "option runs a random password generator. Simply input the number of characters you want generated."
echo ""
echo "[Xrandr Brightness Tool]"
echo "is a simple tool to help you set your Screens Brightness via Xrandr! You can select a Brightness Value of anywhere from '0.25' (25% Brightness) to '1.0' (100% Brightness) although I would not suggest going below a Brightness Value of '0.25' because you might not be able to see. You MIGHT have to configure this tool to set a correct display!"
echo ""
echo "# Notes: If I have time, I MIGHT be adding support for setting a refresh rate and screen size. No promises though."
echo ""
echo "[Unit Converter Menu]"
echo "is a menu for selecting the units you would like converted."
echo ""
echo "[Temperature Converter]"
echo "will take you to a menu where you can select whether to convert Celsius To Fahrenheit or Fahrenheit To Celsius. Most places use Celsius instead of Fahrenheit with the exception being the United States because American's use Fahrenheit. An interesting fact is that -40 Celsius is equal to -40 Fahrenheit!"
echo ""
echo "[Time (UNIT) Converter]"
echo "currently supports converting milliseconds, seconds, minutes, hours, days, weeks, years, decades, centuries and millennia. Please note that the times WILL NOT BE COMPLETELY ACCURATE due to the way BASH interprets numbers. Please do NOT rely on this tool for critical operations in this tools current state!"
echo ""
echo "[Backup Tool]"
echo "will copy/backup files from one directory to another directory within a manually set amount of seconds. You SHOULD have to configure the parameters for how you want this tool to run else, the tool will not know what to do."
echo ""
echo "# Notes: I'm thinking of making this a mirroring tool so if you change files in one place, it copies the changes you've made in the backed up directory?"
echo ""
echo "[File Organizer]"
echo "creates a folder called 'Organized' in the directory that this software started from and moves files of various file extensions into the directory called 'Organized' for your convenience."
echo ""
echo "# Known Issues: A file named, '-Test.png' may NOT work with this tool whereas, 'Test.png' will likely work."
echo ""
echo "[ToolBox Updater]"
echo "is used to update THIS software. WARNING! YOUR edits to LinuxPlusGNUToolBox WILL BE OVERWRITTEN UPON UPDATING LinuxPlusGNUToolBox! In order to combat this issue, a copy of this file WILL be made prior to updating this software in order to preserve your edits (encase you want to keep your edits!) The updater makes a directory based on the version number of the software then copies the current file into '~/Documents/LinuxPlusGNUToolBox_BKs' for your convenience."
echo ""
echo "[World Time]"
echo "Currently supports showing the current time (automatically adjusting for Daylight Savings Time) in: Vietnam, Brazil, Australia, New_Zealand, Japan, China, Ukraine, Italy, United_Kingdom, New_York, Dallas_Texas, Los_Angeles compared to your current time!"
echo ""
echo "[Exit Without Clearing Current Terminal]"
echo "option does not clear the current terminal session and exits the process."
echo ""
echo "[Clear And Exit Current Terminal]"
echo "option clears the current terminal session and exits the process."
echo ""
}
# END fn_manual



# START fn_time_unit_converter_menu
# Time Converter Menu Function
: '
Basic understanding of how this works:
DAYS=1
SECONDSPERDAY=86400
seconds=`expr $DAYS '*' $SECONDSPERDAY`
echo $seconds
'
fn_time_unit_converter_menu() {
clear
echo -ne "
$(greenprint '⟡ Time Converter Command Menu ⟡')

$(greenprint '11)') Millennia
$(greenprint '10)') Centuries
$(greenprint '09)') Decades
$(greenprint '08)') Years
$(greenprint '07)') Months
$(greenprint '06)') Weeks
$(greenprint '05)') Days
$(greenprint '04)') Hours
$(greenprint '03)') Minutes
$(greenprint '02)') Seconds
$(greenprint '01)') Milliseconds
$(greenprint '00)') Return to main menu

Choose an option:  "
read -r ans
case $ans in
11)
fn_time_converter_millennia
;;
10)
fn_time_converter_centuries
;;
9)
fn_time_converter_decades
;;
8)
fn_time_converter_years
;;
7)
fn_time_converter_months
;;
6)
fn_time_converter_weeks
;;
5)
fn_time_converter_days
;;
4)
fn_time_converter_hours
;;
3)
fn_time_converter_minutes
;;
2)
fn_time_converter_seconds
;;
1)
fn_time_converter_milliseconds
;;
0)
./LinuxPlusGNUToolBox.sh
;;
*)
fn_exit
;;
esac
}
# END fn_time_unit_converter_menu



# START mainmenu
mainmenu() {
clear
echo -ne "
$(greenprint "⟡ $software_name $version_check ⟡ ")

$(greenprint "        - COMMAND MENU -")

$(greenprint '10)') 12-24 Hour Time Table
$(greenprint '09)') Password Generator
$(greenprint '08)') Xrandr Brightness Tool
$(greenprint '07)') Unit Converter Menu
$(greenprint '06)') Backup Tool
$(greenprint '05)') File Organizer
$(greenprint '04)') ToolBox Updater
$(greenprint '03)') World Time
$(greenprint '02)') Exit Without Clearing Current Terminal
$(greenprint '01)') Clear And Exit Current Terminal
$(greenprint '00)') Manual

Choose an option: "
read -r ans
case $ans in

10)
fn_12-24hr_timetable
;;
9)
fn_pwgen
;;
8)
fn_brightness_tool
;;
7)
fn_unit_converter_menu
;;
6)
fn_bk_tool
;;
5)
fn_file_organizer
;;
4)
fn_toolbox_updater
;;
3)
fn_worldtime_command_menu
;;
2)
fn_exit
;;
1)
fn_clear
;;
0)
fn_manual
;;
*)
fn_exit
;;
esac
}

mainmenu
# END mainmenu


